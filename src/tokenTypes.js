module.exports = {  
export const SERVIDOR_WEB = "SERVIDOR_WEB";
export const ROTA_HTTP = "ROTA_HTTP";
export const REQUISICAO_HTTP = "REQUISICAO_HTTP";
export const RESPOSTA_HTTP = "RESPOSTA_HTTP";
export const PORTA_SERVIDOR = "PORTA_SERVIDOR";
export const LEFT_PAREN = "LEFT_PAREN";
export const RIGHT_PAREN = "RIGHT_PAREN";
export const LEFT_BRACE = "LEFT_BRACE";
export const RIGHT_BRACE = "RIGHT_BRACE";
export const LEFT_SQUARE_BRACKET = "LEFT_SQUARE_BRACKET";
export const RIGHT_SQUARE_BRACKET = "RIGHT_SQUARE_BRACKET";
export const COMMA = "COMMA";
export const DOT = "DOT";
export const MINUS = "MINUS";
export const PLUS = "PLUS";
export const BIT_AND = "BIT_AND";
export const BIT_OR = "BIT_OR";
export const BIT_XOR = "BIT_XOR";
export const BIT_NOT = "BIT_NOT";
export const COLON = "COLON";
export const SEMICOLON = "SEMICOLON";
export const SLASH = "SLASH";
export const STAR = "STAR";
export const STAR_STAR = "STAR_STAR";
export const MODULUS = "MODULUS";
export const BANG = "BANG";
export const BANG_EQUAL = "BANG_EQUAL";
export const EQUAL = "EQUAL";
export const EQUAL_EQUAL = "EQUAL_EQUAL";
export const GREATER = "GREATER";
export const GREATER_EQUAL = "GREATER_EQUAL";
export const LESS = "LESS";
export const LESS_EQUAL = "LESS_EQUAL";
export const GREATER_GREATER = "GREATER_GREATER";
export const LESSER_LESSER = "LESSER_LESSER";
export const IDENTIFIER = "IDENTIFIER";
export const STRING = "STRING";
export const NUMBER = "NUMBER";
export const E = "E";
export const EM = "EM";
export const CLASSE = "CLASSE";
export const FALSO = "FALSO";
export const FUNÇÃO = "FUNÇÃO";
export const PARA = "PARA";
export const SE = "SE";
export const SENÃOSE = "SENÃOSE";
export const SENÃO = "SENÃO";
export const ESCOLHA = "ESCOLHA";
export const CASO = "CASO";
export const PADRÃO = "PADRÃO";
export const NULO = "NULO";
export const OU = "OU";
export const RETORNA = "RETORNA";
export const SUPER = "SUPER";
export const ISTO = "ISTO";
export const VERDADEIRO = "VERDADEIRO";
export const VAR = "VAR";
export const ENQUANTO = "ENQUANTO";
export const PAUSA = "PAUSA";
export const CONTINUA = "CONTINUA";
export const HERDA = "HERDA";
export const IMPORTAR = "IMPORTAR";
export const FAZER = "FAZER";
export const TENTE = "TENTE";
export const PEGUE = "PEGUE";
export const FINALMENTE = "FINALMENTE";
export const EOF = "EOF";
export const ESCREVA_GROUP = [
    // Português
    "escreva",
    
    // Inglês
    "write",
    
    // Outros idiomas...
    "andika", // Suaíli
    "máilmmi", // Sami
    "imprima", // Espanhol
    "mostre", // Português (alternativa)
    "kokoma", // Yorubá
    "scribe", // Inglês (alternativa)
    "grapho", // Grego
    "xie", // Chinês
    "hono", // Tonganês
    "rexakwaa", // Otjiherero
    "gova", // Tâmil
    "dixava", // Iorubá
    "desenrola", // Português (alternativa)
  ].forEach((word) => {
    tokenTypes[word] = tokenTypes.ESCREVA;
  })
};